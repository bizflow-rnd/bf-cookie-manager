var exec = require('cordova/exec');

exports.getCookieValue = function(url, cookieName, successCallback, errorCallback) {
    exec(successCallback,
                errorCallback,
                'CDVBFCookieManager', 'getCookieValue',
                [url, cookieName]
    );
}

/* exports.setCookieValue = function (url, cookieName, cookieValue, successCallback, errorCallback) {
    exec(successCallback,
                errorCallback,
                'CDVBFCookieManager', 'setCookieValue',
                [url, cookieName, cookieValue]
    );
}

exports.clearCookies = function(successCallback, errorCallback) {
    exec(successCallback,
                errorCallback,
                'CDVBFCookieManager', 'clearCookies',
                []
    );
} */
